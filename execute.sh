#!/usr/bin/env bash

set -euo pipefail

#CLUSTER_NAME=reloader-test-issue
#
#k3d cluster create $CLUSTER_NAME -p 80:80@loadbalancer -p 443:443@loadbalancer

kubectl apply -f reloader/reloader.yaml

kubectl wait --for=condition=available --timeout=600s deploy/reloader-reloader -n reloader

kubectl apply -f "hello-world/nginxv1.yaml"

kubectl wait --for=condition=available --timeout=600s deploy/nginx -n hello-world

sleep 5
curl -qs http://hello-world.127.0.0.1.nip.io/ && echo

kubectl apply -f hello-world/nginxv2.yaml

sleep 5
kubectl wait --for=condition=available --timeout=600s deploy/nginx -n hello-world

curl -qs http://hello-world.127.0.0.1.nip.io/ && echo
